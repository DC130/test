<?php

class H5pay {

    var $api_url;
    var $service = 'TRADE.H5PAY';
    var $version;
    var $merId;
    var $typeId;
    var $tradeNo;
    var $tradeDate;
    var $amount;
    var $notifyUrl;
    var $extra;
    var $summary;
    var $expireTime;
    var $clientIp;
    var $key;
    
    public function send() {
        $data = array(
            'service' => $this->service,
            'version' => $this->version,
            'merId' => $this->merId,
            'typeId' => $this->typeId,
            'tradeNo' => $this->tradeNo,
            'tradeDate' => $this->tradeDate,
            'amount' => $this->amount,
            'notifyUrl' => $this->notifyUrl,
            'extra' => $this->extra,
            'summary' => $this->summary,
            'expireTime' => $this->expireTime,
            'clientIp' => $this->clientIp,
        );
        $urlStr = '';
        foreach ($data as $key => $value) {
            $urlStr .= "$key=$value&";
        }
        $urlStr = substr($urlStr,0,-1);
        $urlStr = $urlStr . $this->key;
        $toMd5 = md5($urlStr);
        $sign = $toMd5;
        $data['sign'] = $sign;
        $post_data = http_build_query($data);
        
        // $en_notifyUrl = urlencode($this->notifyUrl);
        // $result = sprintf("service=%s&version=%s&merId=%s&typeId=%s&tradeNo=%s&tradeDate=%s&amount=%s&notifyUrl=%s&extra=%s&summary=%s&expireTime=%s&clientIp=%s", $this->service, $this->version, $this->merId, $this->typeId, $this->tradeNo, $this->tradeDate, $this->amount, $this->notifyUrl, $this->extra, $this->summary, $this->expireTime, $this->clientIp);
        // $post_data = sprintf("service=%s&version=%s&merId=%s&typeId=%s&tradeNo=%s&tradeDate=%s&amount=%s&notifyUrl=%s&extra=%s&summary=%s&expireTime=%s&clientIp=%s", $this->service, $this->version, $this->merId, $this->typeId, $this->tradeNo, $this->tradeDate, $this->amount, $en_notifyUrl, $this->extra, $this->summary, $this->expireTime, $this->clientIp);

        // $sign = urlencode(md5($result . $this->key));

        // $post_data .= "&sign=$sign";

        $url = $this->api_url;

        return $this->curl_post($url, $post_data);
    }


    public function curl_post($url, $post_data) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST"); //POST
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }
}
