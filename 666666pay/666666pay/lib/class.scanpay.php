<?php

class Scanpay {

    var $api_url;
    var $service = 'TRADE.SCANPAY';
    var $version;
    var $merId;
    var $typeId;
    var $tradeNo;
    var $tradeDate;
    var $amount;
    var $notifyUrl;
    var $extra;
    var $summary;
    var $expireTime;
    var $clientIp;
    var $key;
    
    public function send() {
        $data = array(
            'service' => $this->service,
            'version' => $this->version,
            'merId' => $this->merId,
            'typeId' => $this->typeId,
            'tradeNo' => $this->tradeNo,
            'tradeDate' => $this->tradeDate,
            'amount' => $this->amount,
            'notifyUrl' => $this->notifyUrl,
            'extra' => $this->extra,
            'summary' => $this->summary,
            'expireTime' => $this->expireTime,
            'clientIp' => $this->clientIp,
        );

        //ksort($data); can`t soft
        $urlStr = '';
        foreach ($data as $key => $value) {
            $urlStr .= "$key=$value&";
        }
        $urlStr = substr($urlStr,0,-1);
        $urlStr = $urlStr . $this->key;
        $toMd5 = md5($urlStr);
        $sign = $toMd5;
        $data['sign'] = $sign;
        $post_data = http_build_query($data);
        echo $post_data . '<br>';

        $url = $this->api_url;

        return $this->curl_post($url, $post_data);
    }


    public function curl_post($url, $post_data) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST"); //POST
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }

}
