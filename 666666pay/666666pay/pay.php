<?php
header('Content-type: text/html; charset=utf-8');
include_once("config.php");

//$_REQUEST['type']  $_REQUEST['username'] $_REQUEST['coin']
// type : ALIPAY , WECHAT , QQPAY , UNION, ALIPAYWAP WAP QQWAP 
$tradeNo = $_REQUEST['username'] . "_" . date('YmdHis');

$amount = isset($_REQUEST['coin']) ? trim($_REQUEST['coin']) : '0';

$extra = $_REQUEST['username'] . '_' . $_REQUEST['type'];

$summary = 'iSummary';

$expireTime = '3600'; //自定義

$clientIp = '118.163.21.151'; //$_SERVER['REMOTE_ADDR']

switch ($_REQUEST['type']) {
    case 'ALIPAY' :
        $typeId = '1';
        break;
    case 'ALIPAYWAP' :
        $typeId = '1';
        break;
    case 'WECHAT':
        $typeId = '2';
        break;
    case 'WAP' :
        $typeId = '2';
        break;
    case 'QQPAY' :
        $typeId = '3';
        break;
    case 'QQWAP' :
        $typeId = '3';
        break;
    case 'UNION' :
        $typeId = '4';
        break;
}

if ($_REQUEST['type'] == 'ALIPAY' || $_REQUEST['type'] == 'WECHAT' || $_REQUEST['type'] == 'QQPAY' || $_REQUEST['type'] == 'UNION') {

    include_once("lib/class.scanpay.php");
    $scanpay = new Scanpay();
    $scanpay->api_url = $api_url;
    $scanpay->version = $version;
    $scanpay->typeId = $typeId;
    $scanpay->tradeNo = $tradeNo;
    $scanpay->expireTime = $expireTime;
    $scanpay->amount = $amount;
    $scanpay->extra = $extra;
    $scanpay->summary = $summary;
    $scanpay->merId = $merId;
    $scanpay->tradeDate = date('Ymd');
    $scanpay->clientIp = $clientIp;
    $scanpay->notifyUrl = $scanpay_notifyUrl;
    $scanpay->key = $key;
    $resultData = $scanpay->send();

// 响应吗
    preg_match('{<code>(.*?)</code>}', $resultData, $match);
    $respCode = $match[1];

    if ($respCode == '00') {
// 响应信息
        preg_match('{<desc>(.*?)</desc>}', $resultData, $match);
        $respDesc = $match[1];




        preg_match('{<qrCode>(.*?)</qrCode>}', $resultData, $match);

        $respqrCode = $match[1];
        if (!$respqrCode) {
            header("Location:401.html");
            exit;
        }
        $base64 = $respqrCode;

        ?>

        <!DOCTYPE html>
        <html lang="">
            <head>
                <meta charset="UTF-8">
                <title>
                    <?php
                    if ($_REQUEST['type'] == 'ALIPAY') {
                        echo '支付宝支付';
                    } else {
                        echo '微信支付';
                    }
                    ?></title>
                <script type="text/javascript" src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
                <script type="text/javascript" src="assets/js/base64.min.js"></script>
                <script type="text/javascript" src="assets/js/jquery.qrcode.js"></script>
                <script type="text/javascript">
                    function generateQrcode(base64) {
                        var options = {
                            render: 'image',
                            text: Base64.decode(base64),
                            size: 180,
                            ecLevel: 'M',
                            color: '#222222',
                            minVersion: 1,
                            quiet: 1,
                            mode: 0
                        };
                        $("#qrcode").empty().qrcode(options);
                    }

                    $(function () {
                        var base64 = '<?= $base64 ?>';
                        generateQrcode(base64);

                    })
                </script>
                <style>
                    body{font-family:"Meiryo","Meiryo UI","Microsoft JhengHei UI","Microsoft JhengHei",sans-serif}.pay-content{width:850px;margin:30px auto;display:flex}.pay-list{width:50%;position:relative}.title{background:#111;text-align:center;color:#fff;padding:.7em 0;border-radius:10px;width:100%;margin-top:30px}.title:after{content:" ";position:absolute;width:0;height:0;border-width:10px;border-style:solid;border-color:#111 transparent transparent;top:70px;left:50%}.list-info{padding:10px 30px;font-size:18px}.qrimg{border:1px #111 solid;padding:5px 15px}.qrimg img{width:100%}.pic{width:50%;text-align:right;margin:auto 50px}.pic img{height:600px}.remind{z-index:99;background:#111;color:#fff;text-align:center;margin:auto 50px;margin-top:-10px;font-size:18px}
                </style>
            </head>
            <body>
                <div class="pay-content">
                    <div class="pay-list">
                        <div class="title">
                            二维码有效时长为<span class="numd"><?= $expireTime ?></span>秒，请尽快支付
                        </div>
                        <div class="list-info">
                            <p>订单编号:  <?php echo $tradeNo; ?></p>
                            <p>会员账号:  <?php echo $_REQUEST['username']; ?></p>
                            <p>订单金额:  <?php echo $amount; ?></p>
                        </div>
                        <div class="qrimg">
                            <div id="qrcode"></div>
                        </div>
                        <div class="remind">
                            <?php if ($_REQUEST['type'] == 'ALIPAY') { ?>
                                打开支付宝使用扫一扫支付!
                            <?php } else { ?>
                                打开微信使用扫一扫支付!
                            <?php } ?>
                        </div>
                    </div>
                    <div class="pic">
                        <?php if ($_REQUEST['type'] == 'ALIPAY') { ?>
                            <img src="images/alipay.jpg" alt="支付宝支付">
                        <?php } else { ?>
                            <img src="images/wechat.jpg" alt="微信支付">
                        <?php } ?>
                    </div>
                </div>
                <input type="hidden" name="traceno" value="<?php echo $tradeNo; ?>">
            <!--    <input type="hidden" name="Timestamp" value="--><?php //echo $Timestamp;        ?><!--">-->

            </body>
        </html>


        <?php
    }
} elseif ($_REQUEST['type'] == 'ALIPAYWAP' || $_REQUEST['type'] == 'WAP' || $_REQUEST['type'] == 'QQWAP') {

    include_once("lib/class.h5pay.php");
    $h5pay = new H5pay();
    $h5pay->api_url = $api_url;
    $h5pay->version = $version;
    $h5pay->typeId = $typeId;
    $h5pay->tradeNo = $tradeNo;
    $h5pay->expireTime = $expireTime;
    $h5pay->amount = $amount;
    $h5pay->extra = $extra;
    $h5pay->summary = $summary;
    $h5pay->merId = $merId;
    $h5pay->tradeDate = date('Ymd');
    $h5pay->clientIp = $clientIp;
    $h5pay->notifyUrl = $h5pay_notifyUrl;
    $h5pay->key = $key;
    $resultData = $h5pay->send();


    preg_match('{href="(.*?)">}', $resultData, $match);
    $href = $match[1];
//    if ($href) {
//        header("Location:$href");
//    } else {
//        header("Location:401.html");
//        exit;
//    }
    ?>
    <!DOCTYPE html>
    <html lang="">
        <head>
            <meta charset="UTF-8">
            <title>
                <?php
                if ($_REQUEST['type'] == 'ALIPAY') {
                    echo '支付宝支付';
                } else {
                    echo '微信支付';
                }
                ?></title>
            <script type="text/javascript" src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
            <style>
                body{font-family:"Meiryo","Meiryo UI","Microsoft JhengHei UI","Microsoft JhengHei",sans-serif}.pay-content{width:850px;margin:30px auto;display:flex}.pay-list{width:50%;position:relative}.title{background:#111;text-align:center;color:#fff;padding:.7em 0;border-radius:10px;width:100%;margin-top:30px}.title:after{content:" ";position:absolute;width:0;height:0;border-width:10px;border-style:solid;border-color:#111 transparent transparent;top:70px;left:50%}.list-info{padding:10px 30px;font-size:18px}.qrimg{border:1px #111 solid;padding:5px 15px}.qrimg img{width:100%}.pic{width:50%;text-align:right;margin:auto 50px}.pic img{height:600px}.remind{z-index:99;background:#111;color:#fff;text-align:center;margin:auto 50px;margin-top:-10px;font-size:18px}
            </style>
        </head>
        <body>
            <div class="pay-content">
                <div class="pay-list">
                    <a href="<?= $href ?>"><input type="button" value="支付去"></a>
                </div>
            </div>
            <input type="hidden" name="traceno" value="<?php echo $tradeNo; ?>">
        <!--    <input type="hidden" name="Timestamp" value="--><?php //echo $Timestamp;        ?><!--">-->

        </body>
    </html>

    <?php
}
        