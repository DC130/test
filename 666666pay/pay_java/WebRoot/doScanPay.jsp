<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="com.cloudpay.example.*"%>
<%@page import="java.io.PrintWriter"%>
<%@ page import="java.util.*" %>
<%@ page import="com.cloudpay.example.utils.SignUtil" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>商户支付接口示例 - 扫码支付</title>
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/base64.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.qrcode.js"></script>
    <script type="text/javascript">
        function generateQrcode(url) {
            var options = {
                render: 'image',
                text: Base64.decode(url),
                size: 180,
                ecLevel: 'M',
                color: '#222222',
                minVersion: 1,
                quiet: 1,
                mode: 0
            };
            $("#qrcode").empty().qrcode(options);
        }
    </script>
</head>
<body>
<div class="container">
    <div class="header">
        <h3>支付接口 - 扫码支付示例 - 请求结果：</h3>
    </div>
    <div class="main">
        <div class="response-info">
            <p>
                <%
                    try {
                        // 组织请求数据
                        Map<String, String> paramsMap = new HashMap<String, String>();
                        request.setCharacterEncoding("UTF-8");
                        paramsMap.put("service", Config.APINAME_SCANPAY);
                        paramsMap.put("version", Config.API_VERSION);
                        paramsMap.put("merId", Config.MERCHANT_ID);
                        paramsMap.put("tradeNo", request.getParameter("orderNo"));
                        paramsMap.put("tradeDate", request.getParameter("tradeDate"));
                        paramsMap.put("amount", request.getParameter("amount"));
                        paramsMap.put("notifyUrl", Config.MERCHANT_NOTIFY_URL);
                        paramsMap.put("extra", request.getParameter("merchParam"));
                        paramsMap.put("summary", request.getParameter("tradeSummary"));
                        paramsMap.put("expireTime", request.getParameter("expireTime"));
                        paramsMap.put("clientIp", request.getParameter("clientIp"));
                        paramsMap.put("typeId", request.getParameter("typeId"));
                        
                        
                        String paramsStr = Merchant.generateAlspQueryRequest(paramsMap);
                        System.out.println("扫码"+paramsStr);
                        String signMsg = SignUtil.signData(paramsStr);
                        System.out.println("签名: " + signMsg);
                        paramsStr += "&sign=" + signMsg;

                        String payGateUrl = Config.GATEWAY_URL;

                        // 发送请求并接收返回
                        System.out.println(paramsStr);
                        String responseMsg = Merchant.transact(paramsStr, payGateUrl);
                        System.out.println("===" + responseMsg);

                        // 解析返回数据
                        RefundResponseEntity entity = new RefundResponseEntity();
                        entity.parse(responseMsg);

                        StringBuffer sbHtml = new StringBuffer();
                        if (entity.getRespCode().equals("00")) {
                            sbHtml.append("<p>响应码："+ entity.getRespCode() +"</p>");
                            
                            sbHtml.append("<p>响应描述："+ entity.getRespDesc() +"</p>");
                            sbHtml.append("<p>二维码地址（base64编码）："+ entity.getQrCode() +"</p>");
                            sbHtml.append("<p>签名："+ entity.getSignMsg() +"</p>");
                            sbHtml.append("<p id='qrcode'></p>");
                            sbHtml.append("<script type=\"text/javascript\">generateQrcode('"+ entity.getQrCode() +"')</script>");
                        } else {
                            sbHtml.append(responseMsg);
                        }

                        out.println(sbHtml.toString());

                        out.println("<p style='display:none'>"+ responseMsg +"</p>");

                    } catch (Exception e) {
                        out.println(e.getMessage());
                    }
                %>
            </p>
        </div>
    </div>
</div>
</body>
</html>
