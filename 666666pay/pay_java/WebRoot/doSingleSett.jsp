<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="com.cloudpay.example.*"%>
<%@page import="java.io.PrintWriter"%>
<%@ page import="java.util.*" %>
<%@ page import="com.cloudpay.example.utils.SignUtil" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>商户支付接口示例 - 单笔委托结算</title>
    <link rel="stylesheet" href="assets/css/style.css" />
</head>
<body>
<div class="container">
    <div class="header">
        <h3>支付接口 - 委托结算请求结果：</h3>
    </div>
    <div class="main">
        <div class="response-info">
            <p>
                <%
                    try {
                        // 组织请求数据
                        Map<String, String> paramsMap = new HashMap<String, String>();
                        request.setCharacterEncoding("UTF-8");
                        paramsMap.put("service", Config.APINAME_SETTLE);
                        paramsMap.put("version", Config.API_VERSION);
                        paramsMap.put("merId", Config.MERCHANT_ID);
                        paramsMap.put("tradeNo", request.getParameter("orderNo"));
                        paramsMap.put("tradeDate", request.getParameter("tradeDate"));
                        paramsMap.put("amount", request.getParameter("amount"));
                        paramsMap.put("notifyUrl", request.getParameter("notifyUrl"));
                        paramsMap.put("extra", request.getParameter("merchParam"));
                        paramsMap.put("summary", request.getParameter("tradeSummary"));
                        paramsMap.put("bankCardNo", request.getParameter("bankCardNo"));
                        paramsMap.put("bankCardName", request.getParameter("bankCardName"));
                        paramsMap.put("bankId", request.getParameter("bankId"));
                        paramsMap.put("bankName", request.getParameter("bankName"));
                        paramsMap.put("purpose", request.getParameter("purpose"));

                        String paramsStr = Merchant.generateSingleSettRequest(paramsMap);
                        String signMsg = SignUtil.signData(paramsStr);
                        paramsStr += "&sign=" + signMsg;

                        String payGateUrl = Config.GATEWAY_URL;

                        // 发送请求并接收返回
                        System.out.println(paramsStr);
                        String responseMsg = Merchant.transact(paramsStr, payGateUrl);

                        System.out.println(responseMsg);

                        // 处理返回数据
                        RefundResponseEntity entity = new RefundResponseEntity();
                        entity.parse(responseMsg);

                        StringBuffer sbHtml = new StringBuffer();
                        sbHtml.append("<p>响应码："+ entity.getRespCode() +"</p>");
                        sbHtml.append("<p>响应描述："+ entity.getRespDesc() +"</p>");

                        out.println(sbHtml.toString());

                    } catch (Exception e) {
                        out.println(e.getMessage());
                    }
                %>
            </p>
        </div>
    </div>
</div>
</body>
</html>
