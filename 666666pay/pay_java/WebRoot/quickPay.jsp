<%@ page import="com.cloudpay.example.utils.DateUtil" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>商户支付接口示例 - 快捷支付</title>
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $(".bank-list img").on("click", function() {
                $(".bank-list input[type='radio']").prop("checked", false);
                $(this).prev("input[type='radio']").prop("checked", true);
            })
        })
    </script>
</head>
<body>
    <div class="container">
        <div class="header">
            <h3>支付接口 - 快捷支付示例：</h3>
        </div>
        <div class="main">
            <form target="_blank" method="post" action="quickPay_apply.jsp">
                <ul>
                    <li>
                        <label>订单号</label>
                        <input type="text" name="orderNo" value="<% out.println(DateUtil.getOrderNum()); %>" />
                    </li>
                    <li>
                        <label>交易日期</label>
                        <input type="text" name="tradeDate" value="<% out.println(DateUtil.getDate()); %>" />
                    </li>
                    <li>
                        <label>订单金额</label>
                        <input type="text" name="amount" value="0.01" />
                    </li>
                    <li>
                        <label>商户参数</label>
                        <input type="text" name="merchParam" value="test" />
                    </li>
                    <li>
                        <label>交易摘要</label>
                        <input type="text" name="tradeSummary" value="支付测试" />
                    </li>
                    <li>
                        <label>银行卡类型</label>
                        <select name="cardType">
                            <option value="1" selected>借记卡</option>
                        </select>
                    </li>
                    <li>
                        <label>银行卡卡号</label>
                        <input type="text" name="cardNo" value="" />
                    </li>
                    <li>
                        <label>开户姓名</label>
                        <input type="text" name="cardName" value="" />
                    </li>
                    <li>
                        <label>身份证号</label>
                        <input type="text" name="idCardNo" value="" maxlength="18" />
                    </li>
                    <li>
                        <label>银行预留手机号</label>
                        <input type="text" name="mobile" value="" maxlength="11" />
                    </li>
                    <li style="margin-top: 50px">
                        <label></label>
                        <button type="submit">获取验证码</button>
                    </li>
                </ul>
            </form>
        </div>
    </div>
</body>
</html>
