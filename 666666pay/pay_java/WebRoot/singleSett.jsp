<%@ page import="com.cloudpay.example.utils.DateUtil" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>商户支付接口示例 - 委托结算</title>
    <link rel="stylesheet" href="assets/css/style.css" />
</head>
<body>
    <div class="container">
        <div class="header">
            <h3>支付接口 - 单笔委托结算示例：</h3>
        </div>
        <div class="main">
            <form method="post" action="doSingleSett.jsp">
                <ul>
                    <li>
                        <label>订单号</label>
                        <input type="text" name="orderNo" />
                    </li>
                    <li>
                        <label>交易日期</label>
                        <input type="text" name="tradeDate" />
                    </li>
                    <li>
                        <label>结算通知地址</label>
                        <input type="text" name="notifyUrl" />
                    </li>
                    <li>
                        <label>商户参数</label>
                        <input type="text" name="merchParam" />
                    </li>
                    <li>
                        <label>交易摘要</label>
                        <input type="text" name="tradeSummary" value="委托结算测试" />
                    </li>
                    <li>
                        <label>银行卡卡号</label>
                        <input type="text" name="bankCardNo" />
                    </li>
                    <li>
                        <label>开户姓名</label>
                        <input type="text" name="bankCardName" />
                    </li>
                    <li>
                        <label>银行卡银行代码</label>
                        <input type="text" name="bankId" />
                    </li>
                    <li>
                        <label>银行卡开户行名称</label>
                        <input type="text" name="bankName" />
                    </li>
                    <li>
                        <label>结算金额</label>
                        <input type="text" name="amount" value="10" />
                    </li>
                    <li>
                        <label>用途</label>
                        <select name="purpose">
                            <option value="1">还款</option>
                            <option value="2">借款</option>
                            <option value="6">往来款</option>
                            <option value="10" selected="true">其它合法款项</option>
                        </select>
                    </li>
                    <li style="margin-top: 50px">
                        <label></label>
                        <button type="submit">委托结算</button>
                    </li>
                </ul>
            </form>
        </div>
    </div>
</body>
</html>
