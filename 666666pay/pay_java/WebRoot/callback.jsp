<%@ page import="com.cloudpay.example.*" %>
<%@ page import="com.cloudpay.example.utils.SignUtil" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>商户支付接口示例 - 支付回调</title>
    <link rel="stylesheet" href="assets/css/style.css" />
</head>
<body>
    <div class="container">
        <div class="header">
            <h3>支付接口 - 支付回调示例：</h3>
        </div>
        <div class="main">
            <%
                try {
                    // 获取请求参数，并将数据组织成前面验证源字符串
                    request.setCharacterEncoding("utf-8");

                    String service =  Config.APINAME_NOTIFY;
                    String merId = request.getParameter("merId");
                    String tradeNo = request.getParameter("tradeNo");
                    String tradeDate = request.getParameter("tradeDate");
                    String opeNo = request.getParameter("opeNo");
                    String opeDate = request.getParameter("opeDate");
                    String amount = request.getParameter("amount");
                    String status = request.getParameter("status");
                    String extra = request.getParameter("extra");
                    String payTime = request.getParameter("payTime");
                    String sign = request.getParameter("sign");
                    sign.replaceAll(" ", "\\+");

                    String srcMsg = String
                            .format(
                                    "service=%s&merId=%s&tradeNo=%s&tradeDate=%s&opeNo=%s&opeDate=%s&amount=%s&status=%s&extra=%s&payTime=%s",
                                   service, merId, tradeNo, tradeDate,
                                    opeNo, opeDate, amount, status, extra,
                                    payTime);
                    // 验证签名
                     System.out.println("===================开始接受通知");
                    boolean verifyRst = SignUtil.verifyData(sign, srcMsg);
                    String verifyStatus = "未验证";
                   
                    if (verifyRst) {
                    	 System.out.println("===================验证通过");
                        verifyStatus = "验签通过";
                        /**
                         * 验证通过后，请在这里加上商户自己的业务逻辑处理代码.
                         * 比如：
                         * 1、根据商户订单号取出订单数据
                         * 2、根据订单状态判断该订单是否已处理（因为通知会收到多次），避免重复处理
                         * 3、比对一下订单数据和通知数据是否一致，例如金额等
                         * 4、接下来修改订单状态为已支付或待发货
                         * 5、...
                         */

                        // 判断通知类型，若为后台通知需要回写"SUCCESS"给支付系统表明已收到支付通知
                        // 否则支付系统将按一定的时间策略在接下来的24小时内多次发送支付通知。
                        if (request.getParameter("notifyType").equals("1")) {
                            // 回写‘SUCCESS’方式一： 重定向到一个专门用于处理回写‘SUCCESS’的页面，这样可以保证输出内容中只有'SUCCESS'这个字符串。
                            response.setContentType("text/html; charset=UTF-8");
                            response.sendRedirect("notify.jsp");
                            // 回写‘SUCCESS’方式二： 直接让当前输出流中包含‘SUCCESS’字符串。两种方式都可以，但建议采用第一种。
                            // out.println("SUCCESS");
                        }
                    } else
                    	 System.out.println("===================验证失败");
                        verifyStatus = "验签失败";

                    // 把获得的这些数据显示到页面上，这里只是为了演示，实际应用中应该不需要把这些数据显示到页面上。
                    StringBuffer sbHtml = new StringBuffer();
                    sbHtml.append("<p>接口名称： "+ service +"</p>");
                    sbHtml.append("<p>商户号： "+ merId +"</p>");
                    sbHtml.append("<p>商户订单号： "+ tradeNo +"</p>");
                    sbHtml.append("<p>商户交易日期： "+ tradeDate +"</p>");
                    sbHtml.append("<p>支付平台订单号： "+ opeNo +"</p>");
                    sbHtml.append("<p>支付平台订单日期： "+ opeDate +"</p>");
                    sbHtml.append("<p>支付金额： "+ amount +"</p>");
                    sbHtml.append("<p>订单状态： "+ status +"</p>");
                    sbHtml.append("<p>商户参数： "+ extra +"</p>");
                    sbHtml.append("<p>支付时间： "+ payTime +"</p>");
                    sbHtml.append("<p>验签结果描述： "+ verifyStatus +"</p>");

                    out.print(sbHtml.toString());
                } catch (Exception ex) {
                    out.print(ex.getMessage());
                }
            %>
        </div>
    </div>
</body>
</html>
