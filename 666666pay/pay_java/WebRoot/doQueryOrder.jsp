<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="com.cloudpay.example.*"%>
<%@page import="java.io.PrintWriter"%>
<%@ page import="java.util.*" %>
<%@ page import="com.cloudpay.example.utils.SignUtil" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>商户支付接口示例 - 订单查询</title>
    <link rel="stylesheet" href="assets/css/style.css" />
</head>
<body>
<div class="container">
    <div class="header">
        <h3>支付接口 - 订单查询结果：</h3>
    </div>
    <div class="main">
        <div class="response-info">
            <p>
                <%
                    try {
                        // 组织请求数据
                        Map<String, String> paramsMap = new HashMap<String, String>();
                        request.setCharacterEncoding("UTF-8");
                        paramsMap.put("service", Config.APINAME_QUERY);
                        paramsMap.put("version", Config.API_VERSION);
                        paramsMap.put("merId", Config.MERCHANT_ID);
                        paramsMap.put("tradeNo", request.getParameter("orderNo"));
                        paramsMap.put("tradeDate", request.getParameter("tradeDate"));
                        paramsMap.put("amount", request.getParameter("amount"));

                        String paramsStr = Merchant.generateQueryRequest(paramsMap);
                        String signMsg = SignUtil.signData(paramsStr);
                        System.out.println("签名: " + signMsg);
                        paramsStr += "&sign=" + signMsg;

                        String payGateUrl = Config.GATEWAY_URL;

                        // 发送请求并接收返回
                        System.out.println(paramsStr);
                        String responseMsg = Merchant.transact(paramsStr, payGateUrl);

                        System.out.println(responseMsg);

                        // 解析返回数据
                        QueryResponseEntity entity = new QueryResponseEntity();
                        entity.parse(responseMsg);

                        StringBuffer sbHtml = new StringBuffer();
                        sbHtml.append("<p>响应码："+ entity.getRespCode() +"</p>");
                        sbHtml.append("<p>响应描述："+ entity.getRespDesc() +"</p>");
                        sbHtml.append("<p>支付平台交易日期："+ entity.getAccDate() +"</p>");
                        sbHtml.append("<p>支付平台订单号："+ entity.getAccNo() +"</p>");
                        sbHtml.append("<p>支付订单状态："+ entity.getStatus() +"</p>");
                        sbHtml.append("<p>支付订单号："+ entity.getOrderNo() +"</p>");

                        out.println(sbHtml.toString());

                    } catch (Exception e) {
                        out.println(e.getMessage());
                    }
                %>
            </p>
        </div>
    </div>
</div>
</body>
</html>
