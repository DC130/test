<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>商户支付接口示例 - 首页</title>
    <link rel="stylesheet" href="assets/css/style.css" />
</head>
<body>
    <div class="container">
        <div class="header">
            <h3>支付接口示例：</h3>
        </div>
        <div class="main">
            <p><a class="no_text_decor">一.直连模式</a></p>
            <p><a href="bankPay.jsp" class="no_text_decor">1、网银支付</a></p>
            <p><a href="scanPay.jsp" class="no_text_decor">2、扫码支付</a></p>
            <p><a href="quickPay.jsp" class="no_text_decor">3、快捷支付</a></p>
            <p><a href="queryOrder.jsp" class="no_text_decor">4、支付订单查询</a></p>
            <p><a href="refundOrder.jsp" class="no_text_decor">5、退款申请</a></p>
            <p><a href="singleSett.jsp" class="no_text_decor">6、单笔委托结算</a></p>
            <p><a href="singleSettQuery.jsp" class="no_text_decor">7、单笔委托结算查询</a></p>
            <p><a href="yueQuery.jsp" class="no_text_decor">8、余额查询</a></p>
            <p><a href="cashier.jsp" class="no_text_decor">二.收银台模式</a></p>
            <p><a href="h5Pay.jsp" class="no_text_decor">三.H5支付(请使用手机就浏览器)</a></p>
        </div>
    </div>
</body>
</html>
