<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="com.cloudpay.example.*"%>
<%@page import="java.io.PrintWriter"%>
<%@ page import="java.util.*" %>
<%@ page import="com.cloudpay.example.utils.SignUtil" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>页面跳转中...</title>
</head>
<%
    try {
        // 组织请求数据
        Map<String, String> paramsMap = new HashMap<String, String>();
        request.setCharacterEncoding("UTF-8");
        paramsMap.put("service", Config.APINAME_H5PAY);
        paramsMap.put("version", Config.API_VERSION);
        paramsMap.put("merId", Config.MERCHANT_ID);
        paramsMap.put("tradeNo", request.getParameter("orderNo"));
        paramsMap.put("tradeDate", request.getParameter("tradeDate"));
        paramsMap.put("amount", request.getParameter("amount"));
        paramsMap.put("notifyUrl", Config.MERCHANT_NOTIFY_URL);
        paramsMap.put("extra", request.getParameter("merchParam"));
        paramsMap.put("summary", request.getParameter("tradeSummary"));
        paramsMap.put("expireTime", request.getParameter("expireTime"));
        paramsMap.put("clientIp", request.getParameter("clientIp"));
        paramsMap.put("typeId", request.getParameter("typeId"));

        String paramsStr = Merchant.generateAlspQueryRequestH5(paramsMap);
        System.out.println("收銀台模式"+paramsStr);
        String signMsg = SignUtil.signData(paramsStr);
        System.out.println("签名: " + signMsg);

        String payGateUrl = Config.GATEWAY_URL;
        paramsMap.put("sign", signMsg);

        StringBuffer sbHtml = new StringBuffer();
        sbHtml.append("<form name='bankPaySubmit' action='"
                        + payGateUrl + "' method='post'>");
        for (Map.Entry<String, String> entry : paramsMap.entrySet()) {
            sbHtml.append("<input type='hidden' name='"
                    + entry.getKey() + "' value='" + entry.getValue()
                    + "'/>");
        }
        sbHtml.append("</form>");
        sbHtml.append("<script>document.forms['bankPaySubmit'].submit();</script>");
        response.setCharacterEncoding("utf-8");
        PrintWriter writer = response.getWriter();
        System.out.println(sbHtml);
        writer.print(sbHtml.toString());
        writer.flush();
        writer.close();

    } catch (Exception e) {
        out.println(e.getMessage());
    }
%>
<body>
</body>
</html>
