<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="com.cloudpay.example.*"%>
<%@page import="java.io.PrintWriter"%>
<%@ page import="java.util.*" %>
<%@ page import="com.cloudpay.example.utils.SignUtil" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>商户支付接口示例 - 快捷支付</title>
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
</head>
<body>
<div class="container">
    <div class="header">
        <h3>支付接口 - 快捷支付示例：</h3>
    </div>
    <div class="main">
        <div class="response-info">
            <p>
                <%
                    try {
                        // 组织请求数据
                        Map<String, String> paramsMap = new HashMap<String, String>();
                        request.setCharacterEncoding("UTF-8");
                        paramsMap.put("service", Config.APINAME_QUICKPAY_CONFIRM);
                        paramsMap.put("version", Config.API_VERSION);
                        paramsMap.put("merId", Config.MERCHANT_ID);
                        paramsMap.put("opeNo", request.getParameter("opeNo"));
                        paramsMap.put("opeDate", request.getParameter("opeDate"));
                        paramsMap.put("sessionID", request.getParameter("sessionID"));
                        paramsMap.put("dymPwd", request.getParameter("dymPwd"));

                        String paramsStr = Merchant.generateQuickPayConfirmRequest(paramsMap);
                        String signMsg = SignUtil.signData(paramsStr);
                        System.out.println("签名: " + signMsg);
                        paramsStr += "&sign=" + signMsg;

                        String payGateUrl = Config.GATEWAY_URL;

                        // 发送请求并接收返回
                        System.out.println(paramsStr);
                        String responseMsg = Merchant.transact(paramsStr, payGateUrl);
                        System.out.println("===" + responseMsg);

                        QuickPayConfirmResponseEntity entity = new QuickPayConfirmResponseEntity();
                        entity.parse(responseMsg);

                        out.println(entity.getDesc());

                    } catch (Exception e) {
                        out.println(e.getMessage());
                    }
                %>
            </p>
        </div>
    </div>
</div>
</body>
</html>
