<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="com.cloudpay.example.*"%>
<%@page import="java.io.PrintWriter"%>
<%@ page import="java.util.*" %>
<%@ page import="com.cloudpay.example.utils.SignUtil" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>商户支付接口示例 - 快捷支付</title>
    <link rel="stylesheet" href="assets/css/style.css" />
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
</head>
<body>
<div class="container">
    <div class="header">
        <h3>支付接口 - 快捷支付示例：</h3>
    </div>
    <div class="main">
        <div class="response-info">
            <p>
                <%
                    try {
                        // 组织请求数据
                        Map<String, String> paramsMap = new HashMap<String, String>();
                        request.setCharacterEncoding("UTF-8");
                        paramsMap.put("service", Config.APINAME_QUICKPAY_APPLY);
                        paramsMap.put("version", Config.API_VERSION);
                        paramsMap.put("merId", Config.MERCHANT_ID);
                        paramsMap.put("tradeNo", request.getParameter("orderNo"));
                        paramsMap.put("tradeDate", request.getParameter("tradeDate"));
                        paramsMap.put("amount", request.getParameter("amount"));
                        paramsMap.put("notifyUrl", Config.MERCHANT_NOTIFY_URL);
                        paramsMap.put("extra", request.getParameter("merchParam"));
                        paramsMap.put("summary", request.getParameter("tradeSummary"));
                        paramsMap.put("expireTime", "");
                        paramsMap.put("clientIp", request.getRemoteAddr());
                        paramsMap.put("cardType", request.getParameter("cardType"));
                        paramsMap.put("cardNo", request.getParameter("cardNo"));
                        paramsMap.put("cardName", request.getParameter("cardName"));
                        paramsMap.put("idCardNo", request.getParameter("idCardNo"));
                        paramsMap.put("mobile", request.getParameter("mobile"));
                        paramsMap.put("cvn2", "");
                        paramsMap.put("validDate", "");

                        String paramsStr = Merchant.generateQuickPayApplyRequest(paramsMap);
                        String signMsg = SignUtil.signData(paramsStr);
                        System.out.println("签名: " + signMsg);
                        paramsStr += "&sign=" + signMsg;

                        String payGateUrl = Config.GATEWAY_URL;

                        // 发送请求并接收返回
                        System.out.println(paramsStr);
                        String responseMsg = Merchant.transact(paramsStr, payGateUrl);
                        System.out.println("===" + responseMsg);

                        //解析返回数据
                        QuickPayApplyResponseEntity entity = new QuickPayApplyResponseEntity();
                        entity.parse(responseMsg);

                        if (entity.getCode().equals("00")) {
                            StringBuffer sbHtml = new StringBuffer();
                            sbHtml.append("<form method=\"post\" action=\"quickPay_confirm.jsp\">");
                            sbHtml.append("<input type=\"hidden\"  name=\"opeNo\" value=\""+ entity.getOpeNo() +"\" />");
                            sbHtml.append("<input type=\"hidden\"  name=\"opeDate\" value=\""+ entity.getOpeDate() +"\" />");
                            sbHtml.append("<input type=\"hidden\"  name=\"sessionID\" value=\""+ entity.getSessionID() +"\" />");
                            sbHtml.append("<ul>");
                            sbHtml.append("<li>");
                            sbHtml.append("<label>短信验证码</label>");
                            sbHtml.append("<input type=\"text\" name=\"dymPwd\" maxlength=\"6\" />");
                            sbHtml.append("</li>");
                            sbHtml.append("<li style=\"margin-top: 50px\">");
                            sbHtml.append("<label></label>");
                            sbHtml.append("<button type=\"submit\">确认支付</button>");
                            sbHtml.append("</li>");
                            sbHtml.append("</ul>");
                            sbHtml.append("</form>");

                            out.println(sbHtml);
                        } else {
                            out.println(entity.getDesc());
                        }

                    } catch (Exception e) {
                        out.println(e.getMessage());
                    }
                %>
            </p>
        </div>
    </div>
</div>
</body>
</html>
