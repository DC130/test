<%@ page import="com.cloudpay.example.utils.DateUtil" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>商户支付接口示例 - 扫码支付</title>
    <link rel="stylesheet" href="assets/css/style.css" />
</head>
<body>
    <div class="container">
        <div class="header">
            <h3>支付接口 - 扫码支付示例：</h3>
        </div>
        <div class="main">
            <form method="post" action="doScanPay.jsp">
                <ul>
                    <li>
                        <label>订单号</label>
                        <input type="text" name="orderNo" value="<% out.println(DateUtil.getOrderNum()); %>" />
                    </li>
                    <li>
                        <label>交易日期</label>
                        <input type="text" name="tradeDate" value="<% out.println(DateUtil.getDate()); %>" />
                    </li>
                    <li>
                        <label>订单金额</label>
                        <input type="text" name="amount" value="0.01" />
                    </li>
                    <li>
                        <label>商户参数</label>
                        <input type="text" name="merchParam" value="test" />
                    </li>
                    <li>
                        <label>交易摘要</label>
                        <input type="text" name="tradeSummary" value="支付测试" />
                    </li>
                    <li>
                        <label>超时时间</label>
                        <input type="text" name="expireTime" value="" />
                    </li>
                    <li>
                        <label>客户端IP</label>
                        <input type="text" name="clientIp" value="192.168.1.222" />
                    </li>
                    <li>
                        <label>二维码类型</label>
                        <select name="typeId">
                            <option value="1">支付宝</option>
                            <option value="2">微信</option>
                            <option value="3">qq扫码</option>
                        </select>
                    </li>
                    <li style="margin-top: 50px">
                        <label></label>
                        <button type="submit">获取二维码</button>
                    </li>
                </ul>
            </form>
        </div>
    </div>
</body>
</html>
