<?php
header('Content-type: text/html; charset=utf-8');
require_once("lib/init.php");

$action = 'pay_go.php';

$data_name = array(
    'payType' => 'bankPay', 
    'tradeNo' => 'DC' . date('YmdHis'),
    'amount' => '0.01',
    'extra' => 'other',
    'summary' => 'tool',
    'bankId' =>$bankId,
);
?>

<html>
    <body>
        <form action="<?= $action ?>" method="post" enctype="multipart/form-data">
            <table id="post_data">
                <?php
                foreach ($data_name as $key => $value) {
                ?>
                <tr>
                    <td>
                        <label>
                            <?= $key ?>
                        </label>
                    </td>
                    <td>
                        <?php
                        if (is_array($value) && $key == 'bankId') {
                        ?>
                        <select name="<?= $key ?>" <?= $key == 'channel' ? 'onchange="changeChannel(this)"' : '' ?>>
                            <option value="0">--請選擇--</option>
                            <?php
                            foreach ($value as $value0) {
                            ?>
                            <option value="<?= $value0[1] ?>"><?= $value0[0] ?></option>
                            <?php                         
                            }
                            ?>
                            
                        </select>
                        <?php
                        } else {
                        ?>
                            <input type="text" name="<?= $key ?>" value="<?= $value ?>">
                        <?php
                        }
                        ?>
                    </td>
                </tr>
                <?php
                }
                ?>
                
                
            </table>
            <table>
                <tr>
                    <td>
                    </td>
                    <td>
<!--                    <input type="text" name="username" value="DC">
                        <input type="text" name="type" value="ALIPAY">
                        <input type="text" name="coin" value="1">-->
                        
                        <input type="submit" value="Send">
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>