<?php

include_once("config/pay_config.php");
$payType = $_POST['payType'];
$tradeNo = $_POST['tradeNo'];
$amount = $_POST['amount'];
$extra = $_POST['extra'];
$summary = $_POST['summary'];


if ($payType == 'bankPay') {
    include_once("lib/class.bankpay.php");

    $bankId = $_POST['bankId'];

    $bankpay = new Bankpay();

    $bankpay->api_url = $api_url;
    $bankpay->version = $version;


    $bankpay->tradeNo = $tradeNo;
    $bankpay->amount = $amount;
    $bankpay->extra = $extra;
    $bankpay->summary = $summary;
    $bankpay->bankId = $bankId;
    $bankpay->merId = $merId;
    $bankpay->tradeDate = date('Ymd');
    $bankpay->expireTime = '30';
    $bankpay->clientIp = '118.163.21.151'; //$_SERVER['REMOTE_ADDR'];
    $bankpay->notifyUrl = $bankpay_notifyUrl;
    $bankpay->key = $key;
    $bankpay->send();
} elseif ($payType == 'scanPay') {
    include_once("lib/class.scanpay.php");

    $show_qrcode = 'scanPayResult.php';

    $typeId = $_POST['typeId'];

    $scanpay = new Scanpay();

    $scanpay->api_url = $api_url;
    $scanpay->version = $version;

    $scanpay->typeId = $typeId;

    $scanpay->tradeNo = $tradeNo;
    $scanpay->amount = $amount;
    $scanpay->extra = $extra;
    $scanpay->summary = $summary;

    $scanpay->merId = $merId;
    $scanpay->tradeDate = date('Ymd');
    $scanpay->expireTime = '30';
    $scanpay->clientIp = '118.163.21.151'; //$_SERVER['REMOTE_ADDR'];
    $scanpay->notifyUrl = $scanpay_notifyUrl;
    $scanpay->key = $key;
    $resultData = $scanpay->send();
     
    // 响应吗
    preg_match('{<code>(.*?)</code>}', $resultData, $match);
    $respCode = $match[1];

    // 响应信息
    preg_match('{<desc>(.*?)</desc>}', $resultData, $match);
    $respDesc = $match[1];


    preg_match('{<qrCode>(.*?)</qrCode>}', $resultData, $match);
    if ($match) {
        $respqrCode = $match[1];


        $base64 = $respqrCode;

        header("location:$show_qrcode?base64=$base64&respDesc=$respDesc");
    } else{
        echo $respDesc;
    }
} else {
    
}