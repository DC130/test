<?php

class Bankpay {

    var $service = 'TRADE.B2C';
    var $expireTime = '30';
    var $api_url;
    var $notifyUrl;
    var $version;
    var $merId;
    var $tradeDate;
    var $clientIp;
    var $tradeNo;
    var $amount;
    var $extra;
    var $summary;
    var $bankId;
    var $key;



    public function send() {
        if ($this->if_error()) {
            return 0;
        }
        $en_notifyUrl = urlencode($this->notifyUrl);
        $result = sprintf("service=%s&version=%s&merId=%s&tradeNo=%s&tradeDate=%s&amount=%s&notifyUrl=%s&extra=%s&summary=%s&expireTime=%s&clientIp=%s&bankId=%s", $this->service, $this->version, $this->merId, $this->tradeNo, $this->tradeDate, $this->amount, $this->notifyUrl, $this->extra, $this->summary, $this->expireTime, $this->clientIp, $this->bankId);
        $post_data = sprintf("service=%s&version=%s&merId=%s&tradeNo=%s&tradeDate=%s&amount=%s&notifyUrl=%s&extra=%s&summary=%s&expireTime=%s&clientIp=%s&bankId=%s", $this->service, $this->version, $this->merId, $this->tradeNo, $this->tradeDate, $this->amount, $en_notifyUrl, $this->extra, $this->summary, $this->expireTime, $this->clientIp, $this->bankId);

        $sign = urlencode(md5($result . $this->key));

        $post_data .= "&sign=$sign";

        $url = $this->api_url;
        
        $this->curl_post($url, $post_data);
    }

    public function recive() {
//        header('Content-Type:text/html;charset=GB2312');
//        $orderid = trim($_GET['orderid']);
//        $opstate = trim($_GET['opstate']);
//        $ovalue = trim($_GET['ovalue']);
//        $sign = trim($_GET['sign']);
//
//        //订单号为必须接收的参数，若没有该参数，则返回错误
//        if (empty($orderid)) {
//            die("opstate=-1");  //签名不正确，则按照协议返回数据
//        }
//
//        $sign_text = "orderid=$orderid&opstate=$opstate&ovalue=$ovalue" . $this->key;
//        $sign_md5 = md5($sign_text);
//        if ($sign_md5 != $sign) {
//            die("opstate=-2");  //签名不正确，则按照协议返回数据
//        }
        return 0;
    }

    public function curl_post($url, $post_data) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST"); //POST
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }
    public function if_error() {
        if (1) {
            return 0;
        } else {
            return 1;
        }
    }
}
