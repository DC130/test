﻿<?php

$bankId = array(
    array('中国农业银行', 'ABC'),
    array('中国银行' , 'BOC'),
    array('渤海银行' , 'CBHB'),
    array('中国建设银行' , 'CCB'),
    array('光大银行' , 'CEB'),
    array('兴业银行' , 'CIB'),
    array('招商银行' , 'CMB'),
    array('民生银行' , 'CMBC'),
    array('中信银行' , 'CNCB'),
    array('交通银行' , 'COMM'),
    array('广发银行' , 'GDB'),
    array('华夏银行' , 'HXB'),
    array('中国工商银行' , 'ICBC'),
    array('平安银行' , 'PAB'),
    array('中国邮政储蓄银行' , 'PSBC'),
    array('浦发银行' , 'SPDB'),
);
$qrcodeType = array(
    array('支付宝', '1'),
    array('微信', '2'),
    array('QQ钱包', '3'),
);