<?php

class Scanpay {

    var $api_url;
    var $service = 'TRADE.SCANPAY';
    var $version;
    var $merId;
    var $typeId;
    var $tradeNo;
    var $tradeDate;
    var $amount;
    var $notifyUrl;
    var $extra;
    var $summary;
    var $expireTime = '30';
    var $clientIp;
    var $key;

    public function if_error() {
        $error = 0;
        $msg = "666666pay params error message:";
        if (empty($this->expireTime)) {
            $error = 1;
            $msg .= '<li>expireTime不能为空</li>';
        }
        if (empty($this->api_url)) {
            $error = 1;
            $msg .= '<li>api_url不能为空</li>';
        }
        if (empty($this->notifyUrl)) {
            $error = 1;
            $msg .= '<li>notifyUrl不能为空</li>';
        }
        if (empty($this->version)) {
            $error = 1;
            $msg .= '<li>version不能为空</li>';
        }
        if (empty($this->merId)) {
            $error = 1;
            $msg .= '<li>merId不能为空</li>';
        }
        if (empty($this->tradeDate)) {
            $error = 1;
            $msg .= '<li>tradeDate不能为空</li>';
        }
        if (empty($this->clientIp)) {
            $error = 1;
            $msg .= '<li>clientIp不能为空</li>';
        }
        if (empty($this->tradeNo)) {
            $error = 1;
            $msg .= '<li>tradeNo不能为空</li>';
        }
        if (empty($this->amount)) {
            $error = 1;
            $msg .= '<li>amount不能为空</li>';
        }
        if (empty($this->summary)) {
            $error = 1;
            $msg .= '<li>summary不能为空</li>';
        }
        if (empty($this->typeId)) {
            $error = 1;
            $msg .= '<li>typeId不能为空</li>';
        }
        //若提交参数有误，则提示错误信息
        if ($error) {
            die($msg);
        }
        return $error;
    }

    public function send() {

        $this->if_error();

        $en_notifyUrl = urlencode($this->notifyUrl);
        $result = sprintf("service=%s&version=%s&merId=%s&typeId=%s&tradeNo=%s&tradeDate=%s&amount=%s&notifyUrl=%s&extra=%s&summary=%s&expireTime=%s&clientIp=%s", $this->service, $this->version, $this->merId, $this->typeId, $this->tradeNo, $this->tradeDate, $this->amount, $this->notifyUrl, $this->extra, $this->summary, $this->expireTime, $this->clientIp);
        $post_data = sprintf("service=%s&version=%s&merId=%s&typeId=%s&tradeNo=%s&tradeDate=%s&amount=%s&notifyUrl=%s&extra=%s&summary=%s&expireTime=%s&clientIp=%s", $this->service, $this->version, $this->merId, $this->typeId, $this->tradeNo, $this->tradeDate, $this->amount, $en_notifyUrl, $this->extra, $this->summary, $this->expireTime, $this->clientIp);

        $sign = urlencode(md5($result . $this->key));

        $post_data .= "&sign=$sign";

        $url = $this->api_url;
        return $this->curl_post($url, $post_data);
    }


    public function curl_post($url, $post_data) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST"); //POST
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }

}
