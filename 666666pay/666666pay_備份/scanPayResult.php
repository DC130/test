
<!DOCTYPE html>
<html>
    <head>
        <title>扫码支付</title>
        <script type="text/javascript" src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
        <script type="text/javascript" src="/assets/js/base64.min.js"></script>
        <script type="text/javascript" src="/assets/js/jquery.qrcode.js"></script>
        <script type="text/javascript">
            function generateQrcode(base64) {
                var options = {
                    render: 'image',
                    text: Base64.decode(base64),
                    size: 180,
                    ecLevel: 'M',
                    color: '#222222',
                    minVersion: 1,
                    quiet: 1,
                    mode: 0
                };
                $("#qrcode").empty().qrcode(options);
            }

            $(function () {
                var base64 = '<?php $_GET['base64']?>';
                generateQrcode(base64);

            })
        </script>
    </head>
    <body>


        <div id="qrcode"></div>
        <a id="respDesc"><?php echo $_GET['respDesc']; ?>...</a>
    </body>
</html>