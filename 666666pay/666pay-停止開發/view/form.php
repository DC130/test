<?php
include_once('../config.php');
header("Content-Type:text/html; charset=utf-8");
$action = '../action/pay_submit.php';

$data_name = array(
    'service' => 'TRADE.B2C',
    'version' => '1.0.0.0',
    'merId' => $merId,
    'tradeNo' => 'DC' . date('YmdHis'),
    'tradeDate' => date('Ymd'),
    'amount' => '0.01',
    'ontifyUrl' => $notifyUrl,
    'extra' => 'other',
    'summary' => 'tool',
    'expireTime' => '30',
    'clientIp'=> '118.163.21.151',
    'bankId' =>$bankId,
);
?>

<html>
    <body>
        <form action="<?= $action ?>" method="post" enctype="multipart/form-data">
            <table id="post_data">
                <?php
                foreach ($data_name as $key => $value) {
                ?>
                <tr>
                    <td>
                        <label>
                            <?= $key ?>
                        </label>
                    </td>
                    <td>
                        <?php
                        if (is_array($value) && $key == 'bankId') {
                        ?>
                        <select name="<?= $key ?>" <?= $key == 'channel' ? 'onchange="changeChannel(this)"' : '' ?>>
                            <option value="0">--請選擇--</option>
                            <?php
                            foreach ($value as $key0 => $value0) {
                            ?>
                            <option value="<?= $value0 ?>"><?= $key0 ?></option>
                            <?php                         
                            }
                            ?>
                            
                        </select>
                        <?php
                        } else {
                        ?>
                            <input type="text" name="<?= $key ?>" value="<?= $value ?>">
                        <?php
                        }
                        ?>
                    </td>
                </tr>
                <?php
                }
                ?>
                
                
            </table>
            <table>
                <tr>
                    <td>
                    </td>
                    <td>
<!--                    <input type="text" name="username" value="DC">
                        <input type="text" name="type" value="ALIPAY">
                        <input type="text" name="coin" value="1">-->
                        
                        <input type="submit" value="Send">
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>